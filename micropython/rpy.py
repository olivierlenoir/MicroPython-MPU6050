# Author: Olivier Lenoir - <olivier.len02@gmail.com>
# Created: 2021-12-19 11:06:43
# License: MIT, Copyright (c) 2021 Olivier Lenoir
# Language: MicroPython v1.17
# Project: Roll Pitch Yaw detector
# Description: GY-521 MPU-6050 3-axis gyroscope and acceleration sensor
# Detect if Roll Pitch Yaw is greater than a trigger point


from machine import Pin, I2C
from ustruct import unpack
from utime import sleep_ms

SDA = const(21)
SCL = const(22)
MPU6050_ADDR = const(0x68)
MPU6050_TEMP_OUT = const(0x41)
MPU6050_ACCEL_OUT = const(0x3b)
MPU6050_GYRO_OUT = const(0x43)


class RPY(object):
    def __init__(self, ic2, addr=MPU6050_ADDR):
        self.i2c = i2c
        self.addr = addr
        self.mean = [0, 0, 0]
        self.stddev = [1, 1, 1]
        self._init()

    def _init(self):
        # Wake sensor
        self.i2c.writeto_mem(MPU6050_ADDR, 0x6b, bytes([0]))
        sleep_ms(50)

    def gyro_raw(self):
        #Read and unpack gyro raw data
        return unpack('>hhh', self.i2c.readfrom_mem(self.addr, MPU6050_GYRO_OUT, 6))

    def gyro(self):
        # Read and convert gyro to °/s
        return [a / 131 for a in self.gyro_raw()]

    def gyro_mean_sigma(self, rate=20, p=100):
        # Calculate sensor mean and standard deviation, sentor need to be static
        s = [0, 0, 0]
        sq = [0, 0, 0]
        for _ in range(p):
            d = self.gyro_raw()
            s = [a + b for a, b in zip(s, d)]
            sq = [a + b * b for a, b in zip(sq, d)]
            sleep_ms(rate)
        self.mean = [_s / p for _s in s]
        self.stddev = [(_sq / p - (_s / p) ** 2) ** 0.5 for _s, _sq in zip(s, sq)]

    def gyro_raw_std_score(self):
        # Standardize gyro raw with standard score
        return [(x - m) / p for x, m, p in zip(detector.gyro_raw(), self.mean, self.stddev)]


if __name__ == '__main__':
    i2c = I2C(0, scl=Pin(SCL), sda=Pin(SDA), freq=400000)
    detector = RPY(i2c)
    detector.gyro_mean_sigma()

    while True:
        m = [0, 0, 0]
        print('Reset')
        for _ in range(200):
            # Cumulative gyro
            m = [sum(a) for a in zip(m, detector.gyro_raw_std_score())]
            print(m)
            # trigger point 1000
            if any(a > 1000 for a in map(abs, m)):
                print('You move!')
            sleep_ms(100)
