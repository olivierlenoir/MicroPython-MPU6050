# Author: Olivier Lenoir - <olivier.len02@gmail.com>
# Created: 2021-12-16 19:54:43
# License: MIT, Copyright (c) 2021 Olivier Lenoir
# Language: MicroPython v1.17
# Project: Test GY-521 MPU-6050 3-axis gyroscope and acceleration sensor
# Description:


from machine import Pin, I2C
from ustruct import unpack
from utime import sleep_ms


SDA = const(21)
SCL = const(22)
MPU6050_ADDR = const(0x68)  # 104
MPU6050_TEMP_OUT = const(0x41)  # 2 bytes
MPU6050_ACCEL_OUT = const(0x3b)  # 6 bytes
MPU6050_GYRO_OUT = const(0x43)  # 6 bytes


i2c = I2C(0, scl=Pin(SCL), sda=Pin(SDA), freq=400000)

i2c.scan()  # [104]


# Wake device
i2c.writeto_mem(MPU6050_ADDR, 0x6b, bytes([0]))

for _ in range(20):
    # Read temperature
    temp_raw = i2c.readfrom_mem(MPU6050_ADDR, MPU6050_TEMP_OUT, 2)
    temp_data = unpack('>h', temp_raw)[0]
    temp_c = temp_data / 340 + 36.53
    print(temp_c, '°C')

    # Read accelerometer
    accel_raw = i2c.readfrom_mem(MPU6050_ADDR, MPU6050_ACCEL_OUT, 6)
    accel_data = unpack('>hhh', accel_raw)
    accel_g = [d / 16384 for d in accel_data]
    print(accel_g, 'g')

    # Read gyro
    gyro_raw = i2c.readfrom_mem(MPU6050_ADDR, MPU6050_GYRO_OUT, 6)
    gyro_data = unpack('>hhh', gyro_raw)
    gyro_deg = [d / 131 for d in gyro_data]
    print(gyro_deg, '°/s')

    # Sleep
    sleep_ms(200)
