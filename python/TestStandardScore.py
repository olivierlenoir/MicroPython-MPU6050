"""
Author: Olivier Lenoir - <olivier.len02@gmail.com>
Created: 2021-12-21 21:25:34
License: MIT, Copyright (c) 2021 Olivier Lenoir
Language: Python 3.7.3
Project: Test standard score
"""


from random import gauss
from matplotlib import pyplot as plt


# mean
MU = 121
# standard deviation
SIGMA = 15


# Gaussian distribution data set
data = [gauss(MU, SIGMA) for _ in range(500)]
# Standardize data set with standard score
data_standard_score = [(x - MU) / SIGMA for x in data]


plt.hist(data, 50)
plt.hist(data_standard_score, 50)

plt.show()
