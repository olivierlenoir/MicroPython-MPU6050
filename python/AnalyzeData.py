"""
Author: Olivier Lenoir - <olivier.len02@gmail.com>
Created: 2021-12-29 14:42:43
License: MIT, Copyright (c) 2021 Olivier Lenoir
Language: Python 3.7.3
Project: Analyze data
"""


from random import gauss, randint
from matplotlib import pyplot as plt
from math import atan, asin, acos, degrees

# Datafile
DATAFILE = '../data/MPU6050raw20211229.dat'
# mean
MU = 121
# standard deviation
SIGMA = 15


def standard_score():
    # Gaussian distribution data set
    data = [gauss(MU, SIGMA) for _ in range(100)]
    # Standardize data set with standard score
    data_standard_score = [(x - MU) / SIGMA for x in data]

    plt.hist(data, 50)
    plt.hist(data_standard_score, 50)

    plt.show()


if __name__ == '__main__':
    d = []
    with open(DATAFILE) as data:
        for rec in data.readlines():
            if rec.startswith('('):
                mpu, t = rec[1:-1].split(') ')
                rec = [int(a) for a in mpu.split(', ')]
                rec.append(int(t))
                d.append(rec)

    if False:
        # Individual histogram
        data = list(zip(*d[:1000]))
        for i in (0, 1, 2, 4, 5, 6):
            plt.hist(data[i], 50)  # Blue, Orange, Green, Red, Purple, Brown
            plt.show()

    if False:
        # Euclidean distance of ACCEL and GYRO
        g = [sum(map(lambda x: x ** 2, r[0:2])) ** 0.5 for r in d[:100]]
        plt.hist(g, 50)  # Blue
        g = [sum(map(lambda x: x ** 2, r[4:6])) ** 0.5 for r in d[:100]]
        plt.hist(g, 50)  # Orange
        plt.show()

    if False:
        for r in d[:100]:
            accel = r[:3]
            x, y , z = accel
            a = degrees(atan(x / (y ** 2 + z ** 2) ** 0.5))
            b = degrees(atan(y / (x ** 2 + z ** 2) ** 0.5))
            c = degrees(atan((x ** 2 + y ** 2) ** 0.5 / z))
            print(a, b, c)
            g = (x **2 + y ** 2 + z ** 2) ** 0.5
            a1 = degrees(asin(x / g))
            b1 = degrees(asin(y / g))
            c1 = degrees(acos(z / g))
            print(a1, b1, c1)

    if True:
        for i in range(20):
            accel = [randint(1, 1 << 16) for _ in range(3)]
            x, y , z = accel
            a = degrees(atan(x / (y ** 2 + z ** 2) ** 0.5))
            b = degrees(atan(y / (x ** 2 + z ** 2) ** 0.5))
            c = degrees(atan((x ** 2 + y ** 2) ** 0.5 / z))
            print('atan method :', a, b, c)
            g = (x **2 + y ** 2 + z ** 2) ** 0.5
            a1 = degrees(asin(x / g))
            b1 = degrees(asin(y / g))
            c1 = degrees(acos(z / g))
            print('g method    :', a1, b1, c1)
